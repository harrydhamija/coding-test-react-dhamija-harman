// Articles.js
import React from "react";

const Articles = (props) => {

    const { articlesArray } = props;
    // destructuring from props
    
    return (
        <div className="articles-list">
            {articlesArray.map((article) => {
                return (
                    <div key={article.id}>
                        <h2>{article.name}</h2>
                        <p>{article.content}</p>
                    </div>
                )
            })}
        </div>
    )
}

export default Articles;