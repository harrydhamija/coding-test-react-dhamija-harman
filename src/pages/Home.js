import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';


class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page d\'accueil') }
                </Helmet>

                <div className="home-page content-wrap">
                    <div className="big-bubble"></div>
                    <div className='small-bubble'></div>

                    <div className="infos-block">
                        <h1>04h11</h1>
                        <h2>Le Spécialiste de vos données.</h2>
                    </div>

                    <Link to="/users" className="nav-arrow">
                        <Icon style={{ fontSize: '5rem', color: 'white' }}>arrow_right_alt</Icon>
                    </Link>
                </div>
            </Fragment>
        )
    }
}

export default HomePage;
