import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

import { title } from 'utils';


class E404Page extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page not found') }
                </Helmet>

                <div className="error-page content-wrap">
                    <h1>Page not found</h1>
                    <p>This page does not seem to exist ...</p>
                    <Link to="/">Back to Home</Link>
                </div>
            </Fragment>
        )
    }
}

export default E404Page;
