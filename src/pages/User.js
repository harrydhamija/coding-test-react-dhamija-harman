import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';
import { title } from 'utils';

import { userService } from 'services';
import { userInfo } from 'services';

import Articles from 'components/Articles';

import { useState, useEffect } from 'react';

import dateformat from 'dateformat';

import { useHistory } from 'react-router-dom';

const UserPage = (props) => {

    const { userId } = props.match.params;
    // destructuring userId from props received through router.

    const [users, setUsers] = useState([]);
    // state for list of users from 1st api call

    const [userData, setUserData] = useState({});
    // state for storing userData object from 2nd api call

    const [articlesArray, setArticlesArray] = useState([]);
    // state for storing articles array from data object received from 2nd api call

    useEffect(() => {
        const getUserList = async () => {
            const result = await userService.list();
            setUsers(result);
        }
        // getUserList function to store value from userService.list() imported from userService.js & then updating the state.
        getUserList();
    },[])
    // no dependancy as we only need to run this function once on page load.

    let history = useHistory()
    const handleUserChange = async (event) => {
        const value = await event.target.value
        history.push(`/users/${value}`)
    }
    // useHistory function to navigate through the url's on changing userId & storing them in a varibale to push it in the history object.

    const defaultUserId = userId || 1;
    // declaring a global variable to store value of userId, if userId is undefined, its default value will be 1 & updating the value of <select> element.

    useEffect(() => {
        const getUserInfo = async () => {

            const data = await userInfo(defaultUserId);
            // waiting for userInfo promise to resolve.
            // calling exported function from userService.js & setting parameter value to defaultUserID from event change for second api call
            // after returnig the promise, storing the value into data variable to use it to display user info.
            
            setUserData(data);
            // updating userData state with info from data variable

            setArticlesArray(data.articles)
            // updating articlesArray state with an array containing articles from data variable.
        }
        getUserInfo();
    },[defaultUserId])

    const dateFormat = dateformat(userData.birthdate, "d mmm yyyy");
    const birthdate = dateFormat;
    // changing the format of the birthdate by using dateformat function to match with the mock up.

        return (
        <Fragment>
            <Helmet>
                {title('Secondary Page')}
            </Helmet>

            <div className="user-page content-wrap">
                <Link to='/' className="left-arrow">
                    <Icon style={{ transform: 'rotate(180deg)', fontSize: '5rem', color: 'white' }}>arrow_right_alt</Icon>
                </Link>

                <div className="users-select">
                        <label htmlFor="userList" className="sr-only">User List</label>
                        {/* added label tag for accessibility */}
                            <select name="userList" id="userList" value={defaultUserId} onChange={handleUserChange}>
                            {users.map((user) => {
                                return(
                                    <option value={user.id} key={user.id}>Utilisateur # {user.id}</option>
                                )
                            })}
                            </select>
                </div>
                
                <div className="user-info">
                    <div>
                        <h2>Occupation: <span>{userData.occupation}</span></h2>
                        <h2>Date de naissance: <span>{birthdate} </span></h2>
                    </div>
                </div>

                <Articles articlesArray={articlesArray}></Articles>
                {/* importing component and passing props to it to display data. */}

            </div>
        </Fragment>
    )
}

export default UserPage;
