import { API } from 'constants/api';

export const userService = {
    list
}

function list() {
    const getInfo = { method: 'GET' }
    return new Promise((resolve, reject) => {
        fetch(`${API}/users/`, getInfo)
            .then(result => {
                return result.json()
            })
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                reject(error);
            });
    });
}

// function to make 'get' call with dynamic userId. 
export const userInfo = (number) => {
    const getInfo = { method: 'GET' }

    // number defines the value of userId prop from router.

    return new Promise((resolve, reject) => {
        fetch(`${API}/users/${number}`, getInfo)
            .then(result => {
                return result.json()
            })
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                reject(error);
            });
    });
}
